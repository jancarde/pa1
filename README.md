List.h          : The header file for the List.c file that outlines all the methods for the ADT.
List.c          : The implementation of the List ADT implemented by creating a doubly linked list with a cursor object.
                  This file also creates the List Object and manipulates, access, and creates lists with the various methods.
ListTest.c      : Test file that tests all the methods and prints output to stdout. Each test is labeled in the stdout.
Lex.c           : Utalizes the List ADT implemented in List.c to alphabetize a text file with the alphabetized 
                  version being written to a file. 
Makefile        : Compiles all the code segments properly. Run Make to compile ./Lex and Make ListTest to compile ./ListTest.
README.md       : Table of contents for my PA1 implementation.