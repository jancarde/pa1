#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include "list.h"

// Structs
typedef struct NodeObj* Node;

typedef struct NodeObj {
    ListElement data;
    Node prev;
    Node next;
} NodeObj;

typedef struct ListObj {
    Node front;
    Node back;
    Node cursor;
    int length;
} ListObj;

// Constructors
// Returns reference to new Node object. Initalizes next, prev, and data fields.
Node newNode(ListElement data){
    Node N = malloc(sizeof(NodeObj));
    assert(N != NULL);
    N->data = data;
    N->next = NULL;
    N->prev = NULL;
    return(N);
}

// Frees heap memory pointed to by *pN, and sets *pN to NULL.
void freeNode(Node* pN){
    if (pN != NULL && *pN != NULL){
        free(*pN);
        *pN = NULL;
    }
}

// Returns reference to a new empty List object.
List newList(){
    List L;
    L = malloc(sizeof(ListObj));
    assert(L != NULL);
    L->front = L->back = L->cursor = NULL;
    L->length = 0;
    return(L);
}

// Frees the list from memory.
void freeList(List* pL){
    if(pL!=NULL && *pL!=NULL) { 
        while(length(*pL) != 0) { 
            deleteFront(*pL);
        }
        free(*pL);
        *pL = NULL;
   }
}

// Error Handling
// Handles error messages for the ADT
void printError(char *file, char *functionName, char *errorMsg) {
    fprintf(stderr, "%s Error: calling %s, %s\n", file, functionName, errorMsg);
    exit(EXIT_FAILURE);
}

// Accessors
// Returns length of the List object.
int length(List L){
    if(L == NULL){
        printError("List", "length()", "NULL List reference");
    }
    return(L->length);
}
// Returns front element of the List object.
int front(List L){
    if(L == NULL){
        printError("List", "front()", "NULL List reference");
    }
    return(L->front->data);
}
// Returns back element of the List object.
int back(List L){
    if(L == NULL){
        printError("List", "back()", "NULL List reference");
    }
    return(L->back->data);
}

// Returns the index of the cursor.
int index(List L){
    Node N = NULL;
    int counter = 0;

    if(L == NULL){
        printError("List", "index()", "NULL List reference");
    }

    if(L->cursor == NULL || length(L) == 0){
        return -1;
    } else {
        for(N = L->front; N != NULL; N = N->next){
            if (N->data == L->cursor->data){
                return counter;
            }
            counter++;
        }
        return 1;
    }
}

// Returns the value of the cursor.
int get(List L){
    if(L == NULL){
        printError("List", "get()", "NULL List reference");
    }
    if (L->cursor == NULL) {
        printError("List", "get()", "cursor undefined");
    }

    return(L->cursor->data);
}

// Compares two lists and returns a bool value denoting equality.
bool equals(List A, List B) {
    Node aNode;
    Node bNode;

    if (A == NULL || B == NULL) {
        printError("List", "equals()", "NULL List reference");
    }

    if (length(A) != length(B)) {
        return false;
    }

    aNode = A->front;
    bNode = B->front;

    while (aNode != NULL && bNode != NULL) {
        if (aNode->data != bNode->data) {
            return false;
        }
        aNode = aNode->next;
        bNode = bNode->next;
    }

    return true;
}

// Mutators
// Clears all elements from the list.
void clear(List L){
    Node N = NULL;

    if(L == NULL){
        printError("List", "clear()", "NULL List reference");
    }

    for(N = L->front; N != NULL; N = N->next){
        deleteFront(L);
    }
}

// Sets the value of the cursor
void set(List L, int x){
    if(L == NULL){
        printError("List", "set()", "NULL List reference");
    }
    if(length(L) == 0){
        printError("List", "set()", "empty List");
    }
    if (L->cursor == NULL){
        printError("List", "set()", "cursor undefined");
    }
    L->cursor->data = x;
}

// Moves cursor to the front of the list
void moveFront(List L){
    L->cursor = L->front;
}

// Moves cursor to the back of the list
void moveBack(List L){
    L->cursor = L->back;
}

// Moves cursor to the previous node in the list.
void movePrev(List L){
    if(L->cursor != NULL){
        L->cursor = L->cursor->prev;
    }
}

// Moves cursor to the next node in the list.
void moveNext(List L){
    if(L->cursor != NULL){
        L->cursor = L->cursor->next;
    }
}

// Adds a value to the front of the list.
void prepend(List L, int x){
    Node N = newNode(x);

    if (N == NULL){
        printError("List", "prepend()", "NULL List reference");
    }

    if(L->length == 0){
        L->front = L->back = N;
    } else {
        L->front->prev = N;
        N->next = L->front;
        L->front = N;
    }
    L->length++;
}

// Adds a value to the end of the list
void append(List L, int x){
    Node N = newNode(x);

    if (N == NULL){
        printError("List", "append()", "NULL List reference");
    }

    if(L->length == 0){
        L->front = L->back = N;
    } else {
        L->back->next = N;
        N->prev = L->back;
        L->back = N;
    }
    L->length++;
}

// Adds a value to the list before the cursor.
void insertBefore(List L, int x){
    Node N = newNode(x);

    if (N == NULL){
        printError("List", "insertBefore()", "NULL List reference");
    }
    if (L->cursor == NULL){
        printError("List", "insertBefore()", "cursor undefined");
    }
    if (length(L) == 0){
        printError("List", "insertBefore()", "empty List");
    }
    if(index(L) > 0){
        L->cursor->prev->next = N;
        N->prev = L->cursor->prev;
    } else {
        L->front = N;
    }
    N->next = L->cursor;
    L->cursor->prev = N;
    
    L->length++;
}

// Adds a value to the list after the cursor.
void insertAfter(List L, int x){
    Node N = newNode(x);

    if (N == NULL){
        printError("List", "insertAfter()", "NULL List reference");
    }
    if (L->cursor == NULL){
        printError("List", "insertAfter()", "cursor undefined");
    }
    if (length(L) == 0){
        printError("List", "insertAfter()", "empty List");
    }
    if(index(L) < length(L) - 1){
        L->cursor->next->prev = N;
        N->next = L->cursor->next;
    } else {
        L->back = N;
    }
    N->prev = L->cursor;
    L->cursor->next = N;
    
    L->length++;
}

// Deletes the front item in the list.
void deleteFront(List L){
    Node N = NULL;

    if(L == NULL){
        printError("List", "deleteFront()", "NULL List reference");
    }
    if(length(L) == 0){
        printError("List", "deleteFront()", "empty List");
    }
    if(L->front == L->cursor){
        L->cursor = NULL;
    }

    N = L->front;
    if(length(L) > 1){
        L->front->next->prev = NULL;
        L->front = L->front->next;
    } else {
        L->front = L->back = NULL;
    }
    L->length--;
    freeNode(&N);
}

// Deletes the back item in the list
void deleteBack(List L){
    Node N = NULL;

    if(L == NULL){
        printError("List", "deleteBack()", "NULL List reference");
    }
    if(length(L) == 0){
        printError("List", "deleteBack()", "empty List");
    }
    if(L->back == L->cursor){
        L->cursor = NULL;
    }

    N = L->back;
    if(length(L) > 1){
        L->back->prev->next = NULL;
        L->back = L->back->prev;
    } else {
        L->front = L->back = NULL;
    }
    L->length--;
    freeNode(&N);
}

// Deletes the value the cursor points to in the list.
void delete(List L){
    Node N = NULL;

    if(L == NULL){
        printError("List", "delete()", "NULL List reference");
    }
    if(length(L) == 0){
        printError("List", "delete()", "empty List");
    }
    if(L->cursor == NULL){
        printError("List", "delete()", "cursor undefined");
    }

    N = L->cursor;
    if(length(L) > 1){
        L->cursor->prev->next = L->cursor->next;
        L->cursor->next->prev = L->cursor->prev;
    } else {
        L->front = L->back = NULL;
    }
    L->length--;
    L->cursor = NULL;
    freeNode(&N);
}

// Misc
// Prints the list
void printList(FILE* out, List L){
    Node N = NULL;

    if(L == NULL){
        printError("List", "printList()", "NULL List reference");
    }

    for(N = L->front; N != NULL; N = N->next){
        printf("%i ", N->data);
    }
    printf("\n");
}

// Copies the list and returns a pointer to the copied list object.
List copyList(List L){
    List NL = NULL;
    Node N = NULL;

    if(L == NULL){
        printError("List", "copyList()", "NULL List reference");
    }
    NL = newList();
    
    if(L->length == 0){
        return NL;
    }

    for(N = L->front; N != NULL; N = N->next){
        append(NL, N->data);
    }
    return NL;
}