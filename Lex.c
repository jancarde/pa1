#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "List.h"

#define MAX_LENGTH 256  // Assuming a reasonable max line length

// Function Prototypes
void printError(char *file, char *functionName, char *errorMsg);

int main(int argc, char *argv[]) {
    FILE *in, *out;
    char line[MAX_LENGTH];
    int lineCount = 0;

    // Check command-line arguments
    if (argc != 3) {
        printError("Lex", "main", "Incorrect number of command line arguments");
    }

    // Open files
    in = fopen(argv[1], "r");
    out = fopen(argv[2], "w");
    if (in == NULL || out == NULL) {
        printError("Lex", "main", "Unable to open file");
    }

    // Count lines
    while (fgets(line, MAX_LENGTH, in) != NULL) {
        lineCount++;
    }
    rewind(in);

    // Allocate memory for string array
    char **arr = (char **)malloc(lineCount * sizeof(char *));
    if (arr == NULL) {
        printError("Lex", "main", "Memory allocation failed");
        exit(EXIT_FAILURE);
    }

    // Read lines into array
    for (int i = 0; i < lineCount; i++) {
        arr[i] = (char *)malloc(MAX_LENGTH * sizeof(char));
        if (arr[i] == NULL) {
            printError("Lex", "main", "Memory allocation failed");
            exit(EXIT_FAILURE);
        }
        fgets(arr[i], MAX_LENGTH, in);
    }

    // Create and populate List
    List myList = newList();
    append(myList, 0);  // Start with first index
    for (int i = 1; i < lineCount; i++) {
        moveFront(myList);
        while (index(myList) >= 0 && strcmp(arr[get(myList)], arr[i]) < 0) {
            moveNext(myList);
        }
        if (index(myList) >= 0) {
            insertBefore(myList, i);
        } else {
            append(myList, i);
        }
    }

    // Write sorted output
    for (moveFront(myList); index(myList) >= 0; moveNext(myList)) {
        fprintf(out, "%s", arr[get(myList)]);
    }

    // Cleanup
    for (int i = 0; i < lineCount; i++) {
        free(arr[i]);
    }
    free(arr);
    freeList(&myList);
    fclose(in);
    fclose(out);

    return 0;
}