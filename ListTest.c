#include "list.h"
#include <stdio.h>

int main() {
    int i;
    List L = newList();
    List K = newList();

    printf("\n");

    // Testing append method
    printf("Test append: ");
    for(i = 1; i<=15; i++){
        append(L, i);
    }

    printList(stdout, L);

    // Testing clear method
    printf("Test clear: ");
    clear(L);
    printList(stdout, L);

    // Testing prepend method
    printf("Test prepend: ");  
    for(i = 1; i<=15; i++){
        prepend(L, i);
    }
    printList(stdout, L);

    // Testing length method
    printf("Test length: %d\n", length(L));

    // Testing deleteFront and deleteBack
    printf("Test delete Front and Back: ");    
    deleteFront(L);
    deleteFront(L);
    deleteBack(L);
    deleteBack(L);

    printList(stdout, L);

    // Testing move front, back, and get
    moveFront(L);
    printf("Test moveFront and get: %i\n", get(L));

    moveBack(L);
    printf("Test moveBack and get: %i\n", get(L));

    // Testing move next, and prev
    printf("Parathesis refer to the number of times function is run, cursor starts at front.\n");
    moveFront(L);
    moveNext(L);
    printf("Test moveNext(x1): %i\n", get(L));
    moveNext(L);
    moveNext(L);
    moveNext(L);
    printf("Test moveNext(x3): %i\n", get(L));
    movePrev(L);
    printf("Test movePrev(x1): %i\n", get(L));
    movePrev(L);
    movePrev(L);
    movePrev(L);
    printf("Test movePrev(x3): %i\n", get(L));
    
    // Testing set
    printf("Parathesis refer to the index that was replaced starting with 0.\n");
    moveFront(L);
    moveNext(L);
    moveNext(L);
    moveNext(L);
    set(L, 40);
    printf("Test set(3) with 40: ");
    printList(stdout, L);

    moveNext(L);
    moveNext(L);
    set(L, 83);
    printf("Test set(5) with 83: ");
    printList(stdout, L);

    // Testing delete
    moveFront(L);
    moveNext(L);
    moveNext(L);
    moveNext(L);
    delete(L);
    printf("Test delete(3): ");
    printList(stdout, L);

    // Testing insert before and after
    moveFront(L);
    insertBefore(L, 18);
    printf("Test insert before(0) with 18: ");
    printList(stdout, L);
    insertBefore(L, 36);
    printf("Test insert before(1) with 36: ");
    printList(stdout, L);

    moveBack(L);
    insertAfter(L, 56);
    printf("Test insert after(-1) with 56: ");
    printList(stdout, L);
    insertAfter(L, 72);
    printf("Test insert after(-2) with 72: ");
    printList(stdout, L);

    // Testing index
    moveFront(L);
    moveNext(L);
    moveNext(L);
    moveNext(L);
    moveNext(L);
    printf("Moved cursor to front and preformed moveNext(x4)\n");
    printf("Test index: %i\n", index(L));

    // Testing equals
    clear(L);
    printf("L: ");
    printList(stdout, L);
    printf("K: ");
    printList(stdout, K);
    printf("Test equals L & K: %d\n", equals(L, K));

    for(i = 1; i<=7; i++){
        append(L, i);
        append(K, i);
    }
    printf("L: ");
    printList(stdout, L);
    printf("K: ");
    printList(stdout, K);
    printf("Test equals L & K: %d\n", equals(L, K));

    clear(L);
    clear(K);
    for(i = 1; i<=7; i++){
        append(L, i);
        append(K, 8-i);
    }
    printf("L: ");
    printList(stdout, L);
    printf("K: ");
    printList(stdout, K);
    printf("Test equals L & K: %d\n", equals(L, K));

    //Testing listCopy
    printf("Test copyList L to K:\n");
    freeList(&K);
    K = copyList(L);
    printf("L: ");
    printList(stdout, L);
    printf("K: ");
    printList(stdout, K);

    // Testing freeList
    freeList(&L);
    freeList(&K);
    printf("List has been freed. ");

    return 0;
}
